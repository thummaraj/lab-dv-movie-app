import { Button, Layout, Menu, message, Modal, Spin } from 'antd';
import React, { Component } from 'react';
import RouteMenu from './RouteMenu';
import { connect } from 'react-redux'

const menus = [ "movies", "favorite", "profile"]
const { Header , Content, Footer} = Layout

const mapStateToProps = state => {
    return {
        isShowDialog: state.isShowDialog,
        itemMovieClick: state.itemMovieDetail
    }
}

const mapDispathToProps = dispath => {
    return {
        onDismissDialog: () => 
        dispath({
            type:'dismiss_dialog'
        }),
      onItemMovieClick: item =>
      dispath({
        type: 'click_item',
        payload: item
      })
    }
  }
  
class Main extends Component {
  state = {
    items: [],
    isShowModal: false,
    itemMovie: null,
    pathName: menus[0],
    favItems: []
  };

//   onItemMovieClick = (item) => {
//       //ใช้ state ที่เราตั้งไว้ จากนั้นตามด้วยเราต้องการให้ state รับค่าอะไร 
//       this.setState({
//           isShowModal: true,
//           itemMovie: item})
//   };

  onModalClickOk =() => {
    // this.setState({isShowModal: false})
    this.props.onDismissDialog()
  }
  onModalClickCancel= () => {
    // this.setState({isShowModal: false})
    this.props.onDismissDialog()
  }
  componentDidMount() {
      const jsonStr = localStorage.getItem('list-fav')
      // ถ้า JSON String ออกมาเป็น null พอเอามา parse มันจะไม่ได้ค่า array[] 
    //   solution
      const items = JSON.parse(jsonStr) || [] 
      this.setState({ favItems: items})

      const { pathname} = this.props.location
      // pathName = "/movies"
      var pathName = menus[0]
      if (pathname != '/') {
          //replace value from "/something" to "something"
          pathName = pathname.replace('/','')
          //check existing by includes
          if (!menus.includes(pathName)) pathName = menus[0]
      }
      this.setState({pathName})
    //TODO: get list movie from API
    fetch('https://workshopup.herokuapp.com/movie')
      //change response to be json file
      .then(response => response.json())
      .then(movies => this.setState({ items: movies.results }));
  }

  onMenuClick = (event) => {
      var path = '/'
      if (event.key != ''){
          path = `/${event.key}`
      }
      this.props.history.replace(path)
  }
  onClickFavorite = () => {
      //TODO: save item to localStorage
      const itemClick = this.props.itemMovieClick
      const items = this.state.favItems

      const result = items.find(item => {
          return item.title === itemClick.title
      })
      if(result){
          //TODO: show error
          message.error('This item was added')
      } else {
        items.push(itemClick)
        //TODO: save items to localStorage
        localStorage.setItem('list-fav', JSON.stringify(items))
        message.success('This is a message of success')
        this.onModalClickCancel()
      }

     
  }
  render() {
      const item = this.props.itemMovieClick
    return (
      <div>
        {/* cannot use if/else then use shortIf instead */}
        {this.state.items.length > 0 ? (
            <div style={{ height: '100vh'}}>
            {''}
            <Layout className="layout" style={{ background: 'white'}}>
            <Header
            style={{
                padding: '0px',
                position: 'fixed',
                zIndex:1,
                width: '100%'
            }}
            >
            <Menu
            theme= "ligth"
            mode="horizontal"
            //เพื่อให้มันอยู่ที่เดิม
            defaultSelectedKeys={[this.state.pathName]}
            style={{ lineHeight: '64px'}}
            onClick={e => {
                this.onMenuClick(e)
            }}
            >
            <Menu.Item key={menus[0]}>Home</Menu.Item>
            <Menu.Item key={menus[1]}>Favorite</Menu.Item>
            <Menu.Item key={menus[2]}>Profile</Menu.Item>
            </Menu>
            </Header>
            <Content
            style={{
                padding: '16px',
                marginTop: 64,
                minHeight: '600px',
                justifyContent: 'center',
                alignItems: 'center',
                display: 'flex'
            }}
            >
            <RouteMenu
            items={this.state.items}
            // onItemMovieClick={this.onItemMovieClick}
            />
            </Content>
            <Footer style={{ textAlign: 'center', background: 'white'}}>
            Movie Application Workshop @ CAMT
            </Footer>
            </Layout>
            </div>
        ) : (
          <Spin size="large" />
        )}
        {item != null ? (
            <Modal
            width="40%"
            style={{ maxHeight: '70%'}}
            title={this.props.itemMovieClick.title}
            visible={this.props.isShowDialog}
            onCancel={this.onModalClickCancel}
            footer={[
                <Button
                key="submit"
                type="primary"
                icon="heart"
                size="large"
                shape="circle"
                onClick={this.onClickFavorite}
                />,
                <Button
                key="submit"
                type="primary"
                icon="shopping-cart"
                size="large"
                shape="circle"
                onClick={this.onClickBuyTicket}/>
            ]}
            >
            <img src={item.image_url} style={{width: '100%'}}/>
            <br></br>
            <p>{item.overview}</p>
            </Modal>
        ) :(
            <div/>
        )
    }
    </div>
    )
  }
}

export default connect(
    mapStateToProps,
    mapDispathToProps)(Main)
